//
//  LoadingView.h
//  ShadowNotes
//
//  Created by Allen on 2017/8/17.
//  Copyright © 2017年 Exmart. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoadingviewOpenDelagete <NSObject>

- (void)clickloadingview;

@end

@interface LoadingView : UIView

@property (nonatomic , weak) id<LoadingviewOpenDelagete>delagete;

/**
 大标题
 */
@property (nonatomic , strong) NSArray *titleArray;

/**
 小标题
 */
@property (nonatomic , strong) NSArray *subtitleArray;

/**
 中间图片
 */
@property (nonatomic , strong) NSArray *centerimageArray;



@end
