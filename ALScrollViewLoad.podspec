Pod::Spec.new do |s|
s.name         = 'ALScrollViewLoad'
s.version      = '1.0.0'
s.summary      = 'An ActionSheet like WeChat'
s.homepage     = 'https://git.oschina.net/Allen__yao/ALScrollViewLoading'
s.license      = 'MIT'
s.authors      = {'Allen__yao' => '343956478@qq.com'}
s.platform     = :ios, '6.0'
s.source       = {:git => 'https://git.oschina.net/Allen__yao/ALScrollViewLoading.git', :tag => '1.0.0'}
s.source_files = "ALScrollviewLoad/**/*.{h}"
s.requires_arc = true
end
